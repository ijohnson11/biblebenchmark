﻿/* Isaiah Johnson
 * Bible Benchmark
 * April 22, 2018
 * This is my own work.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BenchmarkBible.Models
{
    public class BibleVerseModel
    {
        
        //Instantiate lists used for dropdown in views upon construction
        public BibleVerseModel()
        {
            TestamentList = new SelectList(new[] { "Old Testament", "New Testament" });
            BookList = new SelectList(new[] {"Genesis", "Exodus", "Leviticus", "Numbers", "Deuteronomy", "Joshua", "Judges", "Ruth",
                                                     "1 Samuel", "2 Samuel", "1 Kings", "2 Kings", "1 Chronicles", "2 Chronicles", "Ezra" , "Nehemiah",
                                                     "Esther", "Job", "Psalms", "Proverbs", "Ecclesiastes", "Song of Solomon", "Isaiah", "Jeremiah",
                                                     "Lamentations", "Ezekiel", "Daniel", "Hosea", "Joel", "Amos", "Obadiah", "Jonah", "Micah", "Nahum",
                                                     "Habakkuk", "Zephaniah", "Haggai", "Zechariah", "Malachi", "Matthew", "Mark", "Luke", "John", "Acts", "Romans", "1st Corinthians", "2nd Corinthians", "Galatians",
                                                    "Ephesians", "Philippians", "Colossians", "1st Thessalonians", "2nd Thessalonians", "1st Timothy",
                                                    "2nd Timothy", "Titus", "Philemon", "Hebrews", "James", "1st Peter", "2nd Peter", "1st John", "2nd John",
                                                    "3rd John", "Jude", "Revelation" });
        }

        //Lists to be used in search and insert views
        public IEnumerable<SelectListItem> TestamentList { get; set; }
        public IEnumerable<SelectListItem> BookList { get; set; }

        [Required(ErrorMessage = "Testament Required.")]
        public string Testament { get; set; }

        [Required(ErrorMessage = "Book Required.")]
        public string Book { get; set; }

        [Required(ErrorMessage = "Chapter Required.")]
        public int Chapter { get; set; }

        [DisplayName("Verse Number")]
        [Required(ErrorMessage = "Verse Number Required.")]
        public int VerseNum { get; set; }

        [DisplayName("Verse Text")]
        [Required(ErrorMessage = "Verse Text Required.")]
        public string VerseText { get; set; }

        
    }
}