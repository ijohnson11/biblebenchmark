﻿/* Isaiah Johnson
 * Bible Benchmark
 * April 22, 2018
 * This is my own work.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BenchmarkBible.Models;
using BenchmarkBible.Services.Business;

namespace BenchmarkBible.Controllers
{
    public class BibleController : Controller
    {

        //Redirects to InsertView
        public ActionResult GoToInsert()
        {
            BibleVerseModel verseModel = new BibleVerseModel();

            return View("InsertView", verseModel);
        }

        //Redirects to VerseSearch
        public ActionResult GoToSearch()
        {
            BibleVerseModel verseModel = new BibleVerseModel();

            return View("VerseSearch", verseModel);

        }

        [HttpPost]
        public ActionResult addBibleVerse(BibleVerseModel bible)
        {
            BibleService bs = new BibleService();

            //Calls business services' addVerse function
            if (bs.AddVerse(bible))
                return View("InsertSuccess");
            else
                return View("InsertFail");
        }

        public ActionResult SearchBibleVerse(BibleVerseModel bible)
        {
            BibleService bs = new BibleService();
            BibleVerseModel model = bs.FindVerse(bible); //Returns a new model 

            //If a null model is returned, query did not return a result
            if (model != null)
                return View("SearchSuccess", model);
            else
                return View("SearchFailure");
        }
    }
}