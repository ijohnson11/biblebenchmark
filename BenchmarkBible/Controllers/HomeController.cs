﻿/* Isaiah Johnson
 * Bible Benchmark
 * April 22, 2018
 * This is my own work.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BenchmarkBible.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View("Index");
        }
    }
}