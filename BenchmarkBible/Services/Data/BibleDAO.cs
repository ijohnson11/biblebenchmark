﻿/* Isaiah Johnson
 * Bible Benchmark
 * April 22, 2018
 * This is my own work.
 */

using BenchmarkBible.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BenchmarkBible.Services.Data
{
    public class BibleDAO
    {
        public bool AddBibleVerse(BibleVerseModel verseModel)
        {
            bool result;

            //Database connection string
            string connection = "Data Source = (localdb)\\MSSQLLocalDB; Initial Catalog = Bible; Integrated Security = True;" +
                " Connect Timeout = 30; Encrypt = False; TrustServerCertificate = True; ApplicationIntent = ReadWrite; MultiSubnetFailover = False";

            try
            {
                //Inserts specified fields into database
                string query = "INSERT INTO dbo.Bible(Testament, Book, Chapter, Verse_Num, Verse_Text) VALUES (@Testament, @Book, @Chapter, @VerseNum, @VerseText)";

                using (SqlConnection cn = new SqlConnection(connection))
                using (SqlCommand cmd = new SqlCommand(query, cn))
                {
                    cmd.Parameters.Add("@Testament", SqlDbType.VarChar, 20).Value = verseModel.Testament;
                    cmd.Parameters.Add("@Book", SqlDbType.VarChar, 25).Value = verseModel.Book;
                    cmd.Parameters.Add("@Chapter", SqlDbType.Int).Value = verseModel.Chapter;
                    cmd.Parameters.Add("@VerseNum", SqlDbType.Int).Value = verseModel.VerseNum;
                    cmd.Parameters.Add("@VerseText", SqlDbType.VarChar, 500).Value = verseModel.VerseText;

                    cn.Open();

                    int rows = cmd.ExecuteNonQuery();
                    if (rows == 1)
                        result =  true;
                    else
                        result = false;

                    cn.Close();
                }
            }
            catch(SqlException e)
            {
                result = false;
            }

            return result;
        }

        public BibleVerseModel FindVerse(BibleVerseModel verseModel)
        {
            BibleVerseModel bible = new BibleVerseModel();

            string connection = "Data Source = (localdb)\\MSSQLLocalDB; Initial Catalog = Bible; Integrated Security = True;" +
                " Connect Timeout = 30; Encrypt = False; TrustServerCertificate = True; ApplicationIntent = ReadWrite; MultiSubnetFailover = False";

            try
            {
                //Query returns all fields of table
                string query = "SELECT * FROM dbo.Bible WHERE Testament = @Testament AND Book = @Book AND Chapter = @Chapter AND Verse_Num = @VerseNum";

                using (SqlConnection cn = new SqlConnection(connection))
                using (SqlCommand cmd = new SqlCommand(query, cn))
                {

                    cmd.Parameters.Add("@Testament", SqlDbType.VarChar, 20).Value = verseModel.Testament;
                    cmd.Parameters.Add("@Book", SqlDbType.VarChar, 25).Value = verseModel.Book;
                    cmd.Parameters.Add("@Chapter", SqlDbType.Int).Value = verseModel.Chapter;
                    cmd.Parameters.Add("@VerseNum", SqlDbType.Int).Value = verseModel.VerseNum;

                    cn.Open();

                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        reader.Read();
                        //Sets bibleVerseModel object's fields based on query results
                        bible.Testament = reader.GetString(1);
                        bible.Book = reader.GetString(2);
                        bible.Chapter = reader.GetInt32(3);
                        bible.VerseNum = reader.GetInt32(4);
                        bible.VerseText = reader.GetString(5);
                    }
                    else
                        bible = null;

                    cn.Close();
                }
            }
            catch (SqlException e)
            {
                // TODO: should log exception and then throw a custom exception
                throw e;
            }
            return bible;
        }
        
    }
}