﻿/* Isaiah Johnson
 * Bible Benchmark
 * April 22, 2018
 * This is my own work.
 */

using BenchmarkBible.Models;
using BenchmarkBible.Services.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BenchmarkBible.Services.Business
{
    public class BibleService
    {
        public bool AddVerse(BibleVerseModel verseModel)
        {
            //Creates DAO to insert verse into db, returns result
            BibleDAO dao = new BibleDAO();
            return dao.AddBibleVerse(verseModel);
        }

        public BibleVerseModel FindVerse(BibleVerseModel verseModel)
        {
            //Creates DAO to query database, returns a BibleVerseModel
            BibleDAO dao = new BibleDAO();
            return dao.FindVerse(verseModel);
        }
    }
}